# Solitude et isolement

## Arte - La solitude a-t-elle du bon ?

> Source [Fuis la solitude ! | Streetphilosophy | ARTE - Youtube](https://www.youtube.com/watch?v=nM4a0J1HSjM).

### Résumé

Le reportage va différencier 2 types de solitude :

- La solitude voulue, qui permet de se reconcentrer sur soi-même (améliorer la connaissance de soi, permet de prendre des décisions sans être influencé, recherche de la sagesse, ...).
- La solitude non voulue, qui peut avoir des effets négatifs sur notre santé.

## Isolement et solitude à l'époque du Covid

> Source [Isolation and solitude in the Covid Era - DOI 10.1016/j.explore.2020.10.010](https://doi.org/10.1016/j.explore.2020.10.010).

### Introduction
Afin d'éviter de propager le covid-19, l'isolement et la solitude ont pris une importance mondiale.

L'**isolement** est un concept qui a longtemps été lié au domaine de la santé. Lorsqu'un patient est atteint d'une maladie infectieuse, il est séparé des autres.

La **solitude** décrit les personnes qui choisissent l'isolement afin de faciliter *une poursuite personnelle* - Par exemple, les écrivains ont souvent besoin de se couper des interférences du monde extérieur afin de se concentrer sur leur travail.

### Avantages et inconvénients
L'universitaire d'Oxford J. R. Thorpe affirme que :

Nous sommes des animaux sociaux, et le fait de passer trop de temps seul a été associé à des niveaux plus élevés de maladies cardiaques et à des taux de mortalité précoce chez les personnes âgées.

Il n'y a pas que des mauvaises nouvelles. Un peu de temps loin du contact humain - en particulier pendant la période chargée des fêtes, où l'on attend de vous que vous participiez à cinq événements sociaux par jour (et que vous soyez de bonne humeur à chacun d'entre eux) - peut en fait avoir de sérieux avantages pour votre santé mentale.

Il liste 6 bénéfices à passer du temps seul :

1. le temps passé seul crée des souvenirs plus forts.
2. Il permet de cultiver de meilleures relations. 
3. il vous rend plus empathique.
4. cela fait de vous un meilleur étudiant.
5. il atténue les sentiments de dépression.
6. cela vous rend plus innovant.

Cependant ce n'est pas ces notions ne sont pas simple, la doctorante en psychologie Eglantine Julle-Daniere dit :

Les sentiments de solitude à long terme peuvent avoir le même impact sur votre corps que le tabagisme ou l'obésité. De plus, l'isolement social prolongé peut entraîner des troubles de stress post-traumatique, l'anxiété, la dépendance, la peur, le suicide, ...

### L'isolement social est-il bon ou mauvais ?
Mme Julle-Daniere différencie le fait d'**être seul** et **se sentir seul**.

- "**Être seul** est l'état physique de ne pas être avec un autre individu, qu'il soit humain ou animal"
- "**Se sentir seul** est un état psychologique qui se caractérise par une expérience pénible survenant lorsque les relations sociales sont perçues comme étant de mauvaise qualité. Quelqu'un peut être seul mais ne pas se sentir seul et quelqu'un peut peut se sentir seule même si elle est entourée de gens."
