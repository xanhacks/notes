# Pyramide de Maslow

## Introduction

La pyramide des besoins, dite **pyramide de Maslow**, est une représentation pyramidale de la hiérarchie des besoins qui interprète la théorie de la motivation basée à partir des observations réalisées dans les années 1940 par le psychologue Abraham Maslow.

![Pyramide de Maslow](https://upload.wikimedia.org/wikipedia/commons/9/9a/Pyramide_des_besoins_de_Maslow.svg)

> Source [Wikipedia - Pyramide des besoins](https://fr.wikipedia.org/wiki/Pyramide_des_besoins).

## Présentation générale

Les besoins s'inscriraient dans le cadre d'une hiérarchie. Tous les besoins sont continuellement présents, mais certains se font plus sentir que d'autres à un moment donné. Lorsqu'un groupe de besoins est satisfait un autre va progressivement prendre la place selon l'ordre hiérarchique. Si un besoin précédent n'est plus satisfait, il redevient prioritaire.

Par exemple :

- Une personne démunie de tout est capable de mettre en péril sa vie pour se nourrir (les besoins physiologiques sont plus importants que les besoins de sécurité).
- Le bizutage, les besoins d'estime ne se font pas sentir avant que les besoins d'appartenance ne soient relativement satisfaits.

## Limites du modèle

- Aucun lien de causalité n'a jamais été prouvé entre les différents besoins (exemple : un employé peut vouloir avoir un grand besoin d'accomplissement avant le besoin d'appartenance au sein de l'entreprise).
- Absence de données scientifiques pour vérifier, justifier, le modèle.
- Abraham Maslow s'est exclusivement limité à étudier la population occidentale.

## Modèles voisins

### Quatorze besoins fondamentaux

Les quatorze besoins fondamentaux représentent un modèle conceptuel en sciences humaines et notamment en soins infirmiers. Ils font partie des courants de pensée infirmière et sont proposés par Virginia Henderson depuis 1947.

```
1. Respirer
2. Boire et manger
3. Éliminer
4. Se mouvoir et maintenir une bonne posture
5. Dormir, se reposer
6. Se vêtir et se dévêtir
7. Maintenir sa température corporelle
8. Être propre et protéger ses téguments
9. Éviter les dangers
10. Communiquer avec ses semblables
11. Agir selon ses croyances et ses valeurs
12. S'occuper en vue de se réaliser
13. Se récréer
14. Apprendre
```

> Source [Wikipedia - Quatorze besoins fondamentaux](https://fr.wikipedia.org/wiki/Quatorze_besoins_fondamentaux_selon_Virginia_Henderson).

### La classification des désirs

La classification des désirs selon Épicure :

- Désirs naturels
	- Nécessaires
		- Pour la vie (nourriture, sommeil)
		- Pour la tranquillité du corps (aponie)
		- Pour le bonheur (ataraxie)
	- Simplement naturels (variation des plaisirs, recherche de l'agréable)
- Désirs vains
	- Artificiels (richesse, gloire...)
	- Irréalisables (désir d'immortalité...)

> Source [Wikipedia - Épicure](https://fr.wikipedia.org/wiki/%C3%89picure#La_classification_des_d%C3%A9sirs).
