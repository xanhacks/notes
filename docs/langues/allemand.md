---
title: Allemand
description: Les bases de la langue allemande.
---

## Vocabulaire

**Salutations :**

| Allemand            | Français      |
|---------------------|--------------|
| Hallo!              | Bonjour !    |
| Guten Morgen!       | Bonjour ! / Bonne matinée ! |
| Guten Tag!          | Bonjour ! / Bonne journée ! |
| Guten Abend!        | Bonsoir !    |
| Tschüss!            | Au revoir !  |

**Expressions courantes :**

| Allemand                     | Français                     |
|------------------------------|------------------------------|
| Danke                        | Merci                        |
| Bitte                        | S'il vous plaît             |
| Entschuldigung               | Excusez-moi / Pardon         |
| Sprechen Sie Englisch?       | Parlez-vous anglais ?        |
| Ich verstehe nicht.          | Je ne comprends pas.         |
| Können Sie mir helfen?       | Pouvez-vous m'aider ?        |
| Wie viel kostet das?         | Combien ça coûte ?           |
| Wo ist...?                  | Où se trouve... ?            |

**Restaurant :**

| Allemand                     | Français                     |
|------------------------------|------------------------------|
| Bitte warten Sie hier. Sie werden platziert. | Veuillez attendre ici. Vous serez placé. |
| Wir gehen in einem Restaurant essen. | Nous allons manger au restaurant. |
| Eine Speisekarte, bitte.     | La carte, s'il vous plaît.   |
| Ich hätte gerne...           | J'aimerais...                |
| Wasser bitte.                | De l'eau, s'il vous plaît.   |
| Die Rechnung, bitte.         | L'addition, s'il vous plaît. |

**Demander son chemin :**

| Allemand                     | Français                     |
|------------------------------|------------------------------|
| Wo ist der Bahnhof?          | Où se trouve la gare ?       |
| Bahnhof / Hauptbahnhof (Hbf) | Gare / Gare centrale         |
| Der Zug nach Stuttgart um 16:52 Uhr kommt auf Gleis 2 an. | Le train en direction de Stuttgart de 16h52 arrive sur la voie 2. |
| Der Zug hat 5 Minuten Verspätung. | Le train a 5min de retard. |
| Wie komme ich zum Hotel XYZ? | Comment je vais à l'hôtel XYZ ? |
| Links / Rechts               | Gauche / Droite              |
| Geradeaus                    | Tout droit                   |
| Wo ist der Ausgang? | Où est la sortie ?   |
| Geöffnet / Geschlossen | Ouvert / Fermé |

**Faire des achats :**

| Allemand                     | Français                     |
|------------------------------|------------------------------|
| Wie viel kostet das?         | Combien ça coûte ?           |
| Kann ich mit Kreditkarte bezahlen? | Puis-je payer par carte de crédit ? |
| Ich möchte das zurückgeben.  | Je voudrais le/la retourner. |
| Öffnungszeiten | Horaire d'ouverture |

**Situation d'urgence :**

| Allemand                     | Français                     |
|------------------------------|------------------------------|
| Hilfe!                       | Au secours !                 |
| Ich brauche einen Arzt.      | J'ai besoin d'un médecin.    |
| Wo ist das nächste Krankenhaus? | Où se trouve l'hôpital le plus proche ? |
| Notrufnummer | Numéro d'urgence |

**Nombres :**

- Eins, Zwei, Drei, Vier, Fünf, ..., Zehn, Zwanzig, Dreißig, Vierzig, ...