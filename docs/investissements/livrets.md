---
title: Livrets d'épargne
description: Choisir ses livrets d'épargne, avantages et inconvénients.
---

# Livrets d'épargne

## Introduction

Les livrets d’épargne offrent plusieurs avantages dans le cadre de l’investissement financier, notamment leur grande liquidité et leurs rendements garantis par l’État. Ils sont utiles pour constituer une épargne de précaution, préparer un apport immobilier ou répondre à des besoins d’épargne à court terme sans risque.

!!! warning

	Les informations présentées sur cette page peuvent être inexactes ou obsolètes, notamment en ce qui concerne les taux d’intérêt et les plafonds des livrets, qui évoluent régulièrement. La plupart des informations sont valides pour l'année 2024.

Sauf mention contraire, vous ne pouvez détenir qu'un livret par type par personne physique.

## Livret A

| **Caractéristiques**                       | **Détails**             |
|--------------------------------------------|-------------------------|
| Plafonds des dépôts                        | 22 950 €                |
| Taux d’intérêt annuel (net)                | 3 %                     |

> [Service Public - Livret A](https://www.service-public.fr/particuliers/vosdroits/F2365)

## Livret de développement durable et solidaire (LDDS)

| **Caractéristiques**                           | **Détails**             |
|------------------------------------------------|-------------------------|
| Plafonds des dépôts                            | 12 000 €                |
| Taux d’intérêt annuel (net)                    | 3 %                     |

> [Service Public - LDDS](https://www.service-public.fr/particuliers/vosdroits/F2368)

## Livret d'épargne populaire (LEP)

Pour avoir le droit d'ouvrir un LEP, votre revenu fiscal de référence ne doit pas dépasser certains plafonds.

**Exemple :** Un plafond de revenu fiscal de référence de 22 419 € maximum pour une part de quotient familial.

| **Caractéristiques**                           | **Détails**             |
|------------------------------------------------|-------------------------|
| Plafonds des dépôts                            | 10 000 €                |
| Taux d’intérêt annuel (net)                    | 4 %                     |

> [Service Public - LEP](https://www.service-public.fr/particuliers/vosdroits/F2367)

## Livret jeune

Vous devez avoir entre 12 et 25 ans pour posséder un livret jeune.

| **Caractéristiques**                           | **Détails**             |
|------------------------------------------------|-------------------------|
| Plafonds des dépôts                            | 1 600 €                 |
| Taux d’intérêt annuel (net)                    | Supérieur à 3 %         |

> [Service Public - Livret jeune](https://www.service-public.fr/particuliers/vosdroits/F2904)

## Plan épargne logement (PEL)

Le PEL a pour objet l'octroi de prêts aux personnes qui ont épargné pendant au moins 4 ans et qui affectent leur épargne au financement de logements destinés à l'habitation principale.

| **Caractéristiques**                           | **Détails**             |
|------------------------------------------------|-------------------------|
| Plafonds des dépôts                            | 61 200 €                |
| Taux d’intérêt annuel                          | 2.25 % Brut             |
| Durée de détention minimum                     | 4 ans                   |
| Versement initial                              | Minimum 225 €           |
| Versement minimum annuel                       | 540 €                   |

A noter que le PEL et le CEL doivent être détenus dans le même établissement.

> [Service Public - PEL](https://www.service-public.fr/particuliers/vosdroits/F16140)

## Compte épargne logement (CEL)

| **Caractéristiques**                           | **Détails**             |
|------------------------------------------------|-------------------------|
| Plafonds des dépôts                            | 15 300 €                |
| Taux d’intérêt annuel                          | 2 % Brut                |
| Versement initial                              | Minimum 300 €           |
| Versement par la suite                         | Minimum 75 €           |

> [Service Public - CEL](https://www.service-public.fr/particuliers/vosdroits/F16136)