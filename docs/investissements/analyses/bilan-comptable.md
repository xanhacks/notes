Le **bilan comptable** est un document financier qui résume les actifs, les passifs et les capitaux propres d'une entreprise à un moment donné.

Il est structuré en deux parties : l'**actif (ce que l'entreprise possède)** et le **passif (ce qu'elle doit)**, dont la somme doit être équilibrée. Le total de l'actif est toujours égal au total du passif

![Bilan](https://www.economie.gouv.fr/files/files/ESPACE-EVENEMENTIEL/FACILECO/Bilan-de-l-entreprise.gif)

**Exemples d'actifs:**

- Actifs tangibles : Propriétés comme terrains, bâtiments, machines, etc.
- Actifs intangibles : Éléments non-physiques tels que brevets, licences, marques, etc.
- Actifs financiers : Investissements tels que participations dans d'autres entreprises.

**Exemples de passifs:**

- Passifs - Capitaux propres : Apports initiaux en numéraire lors de la création de l'entreprise.
- Passifs - Dettes financières : Prêts contractés auprès des établissements bancaires.

**Circulants vs Long terme:**

- Circulants (< 1 an) : Comptes clients, dettes fournisseurs, liquidités, stocks.
- Long terme (> 1 an) : Biens immobiliers, brevets, dettes à long terme.

