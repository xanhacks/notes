---
title: Analyse technique 
description: Présentation de l'analyse technique et de ses objectifs.
---
# Analyse technique
## Définition

L’analyse technique (technical analysis) est une méthode se basant sur l’analyse des variations du cours d’un actif. Sans se soucier des fondamentaux, l’analyse technique consiste à lire et interpréter des graphiques et courbes de prix.
## Aller plus loin 

- [Zonebourse - Analyse technique: arnaque ou martingale ?](https://www.youtube.com/watch?v=umMMG8DsLpQ)
