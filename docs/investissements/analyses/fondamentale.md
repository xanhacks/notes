---
title: Analyse fondamentale 
description: Présentation de l'analyse fondamentale et de ses objectifs.
---
# Analyse fondamentale
## Définition

L’analyse fondamentale (fundamental analysis) est une méthode d’analyse se basant sur les fondamentaux d’un projet (ex: bilan d'une entreprise). Le but est d’étudier en détail un projet pour estimer s’il est sous-évalué ou surévalué.
## Ratios financiers
### Ratios de Solvabilité
#### Ratio d'Autonomie Financière
Mesure l’endettement total d’une entreprise comparativement aux sommes investies par les propriétaires.

Ratio d'Autonomie Financière = Capitaux propres /  Dettes total

- **Capitaux Propres** incluent le capital social, les réserves, les bénéfices non distribués, et d'autres formes de fonds propres.
- **Dettes Totales** incluent toutes les obligations financières de l'entreprise, qu'elles soient à court terme (dettes fournisseurs, emprunts à court terme, etc.) ou à long terme (emprunts à long terme, obligations, etc.).
#### Ratio d'endettement
Évalue le degré de levier financier d'une entreprise et son aptitude à rembourser ses dettes.

Ratio d’Endettement = Capitaux Propres / Dettes Totales​

- **Dettes Totales** incluent toutes les obligations financières de l'entreprise, y compris les emprunts à court et à long terme.
- **Capitaux Propres** représentent les fonds apportés par les actionnaires et les bénéfices non distribués de l'entreprise.
### Ratios de Rentabilité
#### Marge bénéficiaire nette
Mesure la capacité d'une entreprise à convertir ses revenus en profits nets.

Marge bénéficiaire nette = Bénéfices Net / Chiffre d'Affaires Total

- **Bénéfice Net** est le revenu restant après avoir soustrait toutes les dépenses, les impôts et les intérêts des revenus totaux.
- **Chiffre d'Affaires Total** est le revenu total généré par les ventes de biens ou de services de l'entreprise.
#### Marge d'Exploitation
Mesure la proportion des bénéfices d'une entreprise avant intérêts et impôts (EBIT) par rapport à son chiffre d'affaires total.

Marge d'Explotation = Bénéfice d'Exploitation (EBIT) / Chiffre d'Affaires

- **Bénéfice d'Exploitation (EBIT)** est le revenu généré par les activités principales de l'entreprise, avant la déduction des intérêts et des impôts.
- **Chiffre d'Affaires Total** est le revenu total généré par les ventes de biens ou de services.
### Ratios de Liquidité
#### Ratio de liquidité relative
Mesure la capacité d'une entreprise à payer ses dettes à court terme avec ses actifs les plus liquides, c'est-à-dire sans devoir vendre ses stocks.

Ratio de liquidité relative = Actifs Liquide / Passifs à Court Terme

- **Actifs Liquides** incluent les espèces, les équivalents de trésorerie et les comptes clients.
- **Passifs à Court Terme** sont les obligations financières de l'entreprise dues dans l'année à venir.

Un ratio de liquidité relative supérieur à 1 indique que l'entreprise possède suffisamment d'actifs liquides pour couvrir ses obligations à court terme, ce qui est généralement un signe de bonne santé financière.
#### Ratio de liquidité générale
Indique si l’entreprise dispose d’un fonds de roulement suffisant pour respecter ses obligations à court terme, saisir les occasions qui s’offrent à elle et obtenir des conditions de crédit favorables.

Ratio de liquidité générale = Actifs à Court Terme / Passifs à Court Terme 

- **Actifs à Court Terme** incluent les espèces, les équivalents de trésorerie, les comptes clients, les stocks, et d'autres actifs qui sont attendus être liquidés ou utilisés dans l'année.
- **Passifs à Court Terme** sont les obligations financières de l'entreprise dues dans l'année à venir, incluant les dettes fournisseurs, les emprunts à court terme, et d'autres passifs à court terme.
## Aller plus loin

- [Analyse technique vs Analyse fondamentale](https://cryptanalyst.fr/analyse-technique-vs-analyse-fondamentale/)
