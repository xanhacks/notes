---
title: Investissements financiers
description: Les bases de l'investissement financier.
---

# Introduction à l'investissement financier

## Pourquoi investir ?

Le taux d’intérêt du Livret A a tendance à être inférieur à celui de l’inflation. Si vous laissez *dormir* votre argent sur un Livret A, vous perdez donc du pouvoir d’achat au fil du temps. À titre d’exemple, posséder 1 000 € en janvier 1980 équivaut à une valeur de 3 247 € à la fin de 2020.  

En revanche, le rendement moyen de la bourse à long terme (sur plusieurs années ou décennies) se situe généralement entre 6 % et 10 % par an. Ce rendement peut toutefois varier considérablement d’une année à l’autre.  

**Exemple :** Le [CAC 40 Gross Total Return](https://www.boursorama.com/bourse/indices/cours/1rPPX1GR/) (CAC 40 avec dividendes réinvestis) a affiché une performance de +75 % sur 5 ans (de août 2016 à août 2021). De leur côté, le [NASDAQ](https://www.boursorama.com/cours/NDAQ/) et le [S&P 500](https://www.boursorama.com/bourse/indices/cours/%24INX/) ont respectivement progressé de +700 % et +270 % sur 10 ans (de août 2011 à août 2021).  

Contrairement au travail, qui consiste en un échange de temps contre de l’argent, l’investissement offre l’opportunité de générer des revenus avec un effort en temps très limité.

> Les pauvres et la classe moyenne travaillent pour de l'argent. Les riches font en sorte que l'argent travaille pour eux - Robert Kiyosaki

## Investir c'est risqué ?

L'investissement financier comporte un certain degré de risque, pouvant aller jusqu'à la perte totale du capital investi. Toutefois, il est possible de réduire ce risque de manière significative en appliquant les deux méthodes suivantes :

- **Investir à long terme :** En adoptant une stratégie d'investissement à long terme, vous augmentez vos chances de profiter de la croissance durable des marchés financiers tout en limitant les risques liés à la volatilité à court terme. Vous bénéficiez également de l'effet des intérêts composés (l'effet boule de neige).

- **Diversifier son portefeuille :** En diversifiant vos investissements, vous réduisez le risque global, notamment en cas de défaillance ou de perte d'une partie de votre portefeuille. Comme le dit l'adage : "Ne mettez pas tous vos œufs dans le même panier."

Avant de commencer à investir, gardez en tête ces principes fondamentaux :

- Les performances passées ne garantissent pas les performances futures.  
- N'investissez que l'argent que vous êtes prêt à perdre.

> Y a rien de facile dans l'argent facile - Ninho

## Épargne de précaution

L’épargne de précaution est une somme d’argent mise de côté pour faire face aux dépenses imprévues. Elle joue un rôle essentiel dans la tranquillité d’esprit, car elle est immédiatement disponible pour vous aider à gérer les aléas de la vie quotidienne. Cette épargne vous évite de recourir à un découvert bancaire ou à un crédit à la consommation. Elle permet également de préserver vos investissements à long terme.

L'épargne de précaution est généralement conservée sous forme de liquidités, comme des espèces ou sur un compte bancaire facilement accessible (par exemple : Livret A, compte courant, etc.). Il est recommandé d'épargner au minimum l'équivalent de trois mois de dépenses courantes pour constituer une épargne de précaution solide.

**Exemple :** Si vos dépenses mensuelles s'élèvent à 2 000 €, une épargne de précaution couvrant six mois de dépenses nécessite 12 000 € de liquidités (6 × 2 000 €).

> En savoir plus : [L’épargne de précaution, la clé de votre tranquillité](https://particuliers.societegenerale.fr/nos-conseils/epargner/epargne-precaution).

## Comment établir un budget ?

Pour déterminer le montant que vous souhaitez investir, il est essentiel de savoir gérer votre budget afin de dégager une épargne.  

Certaines banques ou courtiers en ligne permettent de commencer à investir avec seulement quelques dizaines d'euros par mois. L'important est de créer une habitude d'investissement le plus tôt possible pour tirer parti des avantages de l'investissement à long terme.  

Pour vous faciliter la tâche, vous pouvez utiliser un tableau Excel ou un autre outil de suivi financier. Ce tableau peut regrouper vos dépenses fixes (loyer, assurance, forfait téléphonique, etc.), vos dépenses quotidiennes (nourriture, transports, loisirs, etc.) et vos revenus (salaire, aides sociales, etc.).  

Une fois vos finances bien organisées, vous serez en mesure d'estimer un montant mensuel à investir. Pour augmenter ce montant, deux leviers s'offrent à vous : augmenter vos revenus ou réduire vos dépenses. L'objectif n'est pas de renoncer aux plaisirs de la vie, mais d'apprendre à dépenser votre argent de manière plus réfléchie et stratégique.

## Ressources

L’investissement financier a connu une popularité exponentielle depuis le confinement lié à la pandémie de Covid-19. Cette tendance s’explique notamment par la facilité d’accès offerte par l’émergence des courtiers en ligne et l’engouement pour cette pratique sur les réseaux sociaux. Bien que cela soit globalement une bonne chose, cette popularité génère une abondance d’informations en ligne, rendant parfois difficile de distinguer le vrai du faux. Certaines personnes promettent des gains souvent exagérés, voire erronés. Voici, selon moi, une liste de sources d’information fiables et intéressantes pour mieux comprendre les finances personnelles :

- [Zone Bourse - Youtube](https://www.youtube.com/@ZonebourseFR)
- [Xavier Delmas - Youtube](https://www.youtube.com/@xavierdelmasinvest)
- [Finary - Youtube](https://www.youtube.com/@Finary)
- [r/vosfinances - Reddit](https://www.reddit.com/r/vosfinances/)
- [Forum Finance Finary](https://community.finary.com/)