---
title: Introduction aux actifs
description: Présentation des différents actifs financiers. 
---

# Type d'actifs

## Actions

Une action représente un titre de propriété d'une entreprise. C'est une source de financement à long terme pour les entreprises.

Pour l'investisseur, il existe deux moyens pour gagner de l'argent :

- Plus-value sur la revente.
- Dividendes (redistribution des bénéfices de l'entrepise).

### Avantages

- Bonne performance.

### Inconvénients

- Risque relativement élevé.

### Pour aller plus loin

- [Zonebourse - Combien d'actions en portefeuille ?](https://www.youtube.com/watch?v=d-ZtKSIITDg)
- [Zonebourse - 5 pièges à éviter sur les dividendes](https://www.youtube.com/watch?v=3RCrORPq_sE)
- [Zonebourse - Faut-il acheter les plus fortes baisses ?](https://www.youtube.com/watch?v=Y-bdudcRheI)

## ETF (Exchange-traded fund)

> En français : FNB (Fonds négocié en bourse)

Un ETF a la même composition que l’indice boursier qu’il reproduit. Il varie donc à la hausse ou à la baisse comme l’indice qu’il reproduit.
 Par exemple, au lieu d'acheter individuellement toutes les actions du CAC40, vous pouvez acheter un ETF qui est indicé sur celui-ci.

### Avantages

- Moins de frais qu'un OPCVM (SICAV ou FCP) car il est géré automatiquement.
- Diversification intéressante car c'est un ensemble de valeurs.
- Montant des ETFs souvent plus abordables que l'ensemble des valeurs qu'ils répliquent.
- Bonne performance.

### Inconvénients

- Risque relativement élevé (notamment selon la diversification des valeurs de l'ETF).

Exemple : Vous achetez un ETF répliquant une cinquantaine d'actions du même secteur d'activité. Si celui-ci se porte mal, les performances vont être lourdement impactées (malgré le nombre important d'actions).

### Type de réplication

**Physique vs Synthétique**

- La réplication physique (en - physical replication) réplique la performance d'un indice (ex: CAC40), via des investissements réalisés directement dans les entreprises qui composent les indices cibles. Chaque titre se voit, dès lors, accorder la même proportion que dans l'indice.
- La réplication synthétique (en - synthetic replication) fonctionne avec un mécanisme plus complexe : il investit dans un panier d'actifs divers, appelé le *collatéral* (garantie servant à couvrir le risque de crédit), mais reproduit la performance d'un indice boursier spécifique via un contrat *swap* de performance ou *total return swap*, passé avec une contrepartie. L'ETF ne détient pas directement les actifs que l'indice est censé reproduire.

### Type de distribution des dividendes

**Distribuants vs Capitalisants**

- Les ETF distribuants (DISTributing ETFs) transfèrent vos revenus (dividendes) directement sur votre compte de placement où vous pouvez les retirer. Ces revenus peuvent être versés selon plusieurs rythmes de distribution, mensuel, trimestriel ou annuel.
- Les ETF capitalisants (ACCumulating ETFs) réinvestissent automatiquement vos revenus dans le fonds, sans frais supplémentaires.

### Fiscalité et dividendes

Sur un CTO (compte titres), les dividendes étant soumis à l'impôt, il est préférable de choisir des ETFs capitalisants qui augmentent les intérêts composés qui ne seront imposés qu'à la revente.

### Éligiblité de l'ETF dans le PEA

Certains ETFs permettent de contourner la contrainte du PEA d'être investis à 75% en valeur européenne grâce à un montage financier. On parle d'ETF synthétique par opposition à physique s'il ne détient pas physiquement les titres mais simule leur détention.

### Pour aller plus loin

Sur la durée, les ETFs répliquant un indice (ex: S&P 500) ont des meilleurs résultats que la plupart des gérants essayant de battre cet indice. C'est donc un gain d'argent et de temps.

Liste des ETFs par capitalisation boursière : [lien](https://etfdb.com/compare/market-cap/).

- [Wikipedia - Fonds négocié en bourse](https://fr.wikipedia.org/wiki/Fonds_n%C3%A9goci%C3%A9_en_bourse)
- [Zonebourse - Qui peut battre les indices ?](https://www.youtube.com/watch?v=_U9JLGG7p7g)
- [Zonebourse - L'investissement passif n'existe pas !](https://www.youtube.com/watch?v=ylw17vGIxYk)
- [Zonebourse - Les 6 points à vérifier avant d'acheter un ETF](https://www.youtube.com/watch?v=BOwye2m6yzA)

## Obligations

Une obligation est un prêt d'argent avec une durée et un taux d'intérêt. Vous pouvez prêter de l'argent à un État ou une entreprise.

### Avantages

- Le risque de non-remboursement du prêt à l'échéance est faible.

### Inconvénients 

- Les performances sont faibles.

## Cryptomonnaies

Une cryptomonnaie est une monnaie numérique échangeable de pair à pair, sans nécessité de banque centrale, utilisable au moyen d'un réseau informatique décentralisé.

### Avantages

- Performance élevée.
- Possibilité de faire du staking (taux d'intérêt élévés).
- En cours de popularisation.

### Inconvénients

- Volatilité élevée.
- Diversification compliquée (les cryptomonnaies sont souvent corrélées entre elles).
- Actif à la mode, risque de bulle.
- Risque règlementaire (fait peur à certains États).
- Risque climatique dû à la forte consommation d'énergie.

### Types de cryptomonnaies

- Stable coin (réplique l'indice du $, ou autres devises)
- Alt coin (autre que le BTC)
- Shit coin (faibles capitalisations)

### Pour aller plus loin

- [Zonebourse - Combien investir dans les cryptos ?](https://www.youtube.com/watch?v=AbG9Yrf0_O4)
- [Zonebourse - Le Bitcoin va-t-il tuer l'or ?](https://www.youtube.com/watch?v=wJNDYDPaP8k)

