---
title: Compte d'investissements
description: Choisir son compte d'investissements, avantages et inconvénients.
---

# Comptes d'investissements

## Introduction

Un compte d'investissement est un type de compte bancaire qui permet aux individus d'investir de l'argent dans différents types de placements (actions, obligations, fonds commun, ...). Il existe différents types de comptes, ils possèdent chacun leurs avantages et inconvénients.

## CTO - Compte Titre Ordinaire

Le compte titre ordinaire (CTO) est une enveloppe qui permet de déposer et de gérer un portefeuille d'actifs.

### Avantages

- Éligible à beaucoup de types d'actifs (actions, ETF, obligations, OPCVM, SICAV, FCP...).
- Le compte n'est pas plafonné à un certain montant.

### Inconvénients

- Les plus-values du CTO sont fortement imposées (environ 30 %, dont 12.8 % d’impôt sur le revenu et 17.20 % de prélèvements sociaux), l’année suivant celle de la cession.

## PEA / PEA-PME - Plan d'épargne en actions

Le plan épargne action (PEA) est un produit d'épargne réglementé. Il permet d'acquérir et de gérer un portefeuille d'actions d'entreprises européennes, tout en bénéficiant, sous conditions, d'une exonération d'impôt.

Il y a 2 types de PEA :

- PEA classique
- PEA-PME : dédié aux titres des PME (petite et moyenne entreprise) et des ETI (entreprise de taille intermédiaire).

### Avantages

- Moins d'impôts. Les dividendes et les plus-values que procurent les placements ne sont pas imposables, à condition d'être réinvestis dans le PEA. On évite donc la taxe de 12,8 %.

### Inconvénients

- Le compte est clôturé en cas de retrait d'argent avant les 5 ans d'ouverture du compte.
- Seulement éligible pour :
	- Des actions d’entreprises européennes.
	- Certains ETF, SICAV et FCP.
- Les comptes sont plafonnés :
    - PEA : 150 000 €
    - PEA-PME : 225 000 €
	- PEA-jeune : 20 000 €

### À savoir

- Le PEA-PME et le PEA classique sont cumulables. Mais la somme totale versée sur ces 2 plans par un même titulaire ne peut pas dépasser 225 000 €. Même en cas de cumul, le plafond du PEA classique ne doit pas dépasser 150 000 €.
- Les revenus du PEA sont quand même soumis aux 17,20 % de prélèvements sociaux (CSG, CRDS).
- Un seul PEA peut être ouvert par personne majeure.
- Les enfants mineurs ou majeurs rattachés au foyer fiscal de leurs parents peuvent aussi ouvrir un PEA, nommés PEA-jeune.

> Source [service-public.fr - F2385](https://www.service-public.fr/particuliers/vosdroits/F2385) et [service-public.fr - F22449](https://www.service-public.fr/particuliers/vosdroits/F22449).

## Assurance vie

Il existe trois types de contrats d’assurance-vie : l'assurance en cas de vie, l'assurance en cas de décès et un contrat mixte de vie et décès. Les assurances-vie garantissent le versement d'un capital ou d'une rente au souscripteur ou au bénéficiaire désigné dans le contrat.

L'assurance en cas de décès constitue une garantie pour les proches de l'assuré, alors que l'assurance en cas de vie est davantage utilisée comme placement, l'assuré pouvant être lui-même le bénéficiaire du contrat.

### Avantages

- Les contrats de plus de 8 ans bénéficient, selon l'option choisie d'un :
	- Abattement de 4 600 € (ou 9 200 € en cas d'imposition commune) sur l'impôt sur le revenu.
	- Prélèvement libératoire au taux réduit de 7,5 %.

### Inconvénients

- Les frais sont plus élevés. (À partir du 1er juin 2022 les producteurs et les distributeurs d'épargnes retraite ou de contrat d'assurance-vie devront afficher les frais sur leur site dans un tableau standardisé)

> Source [economie.gouv.fr - Assurance vie](https://www.economie.gouv.fr/cedef/assurance-vie) et [service-public.fr - Contrat d'assurance-vie](https://www.service-public.fr/particuliers/vosdroits/F15274).

## Plan d'épargne entreprise (PEE / PEG / PEI)

Un PEE est un produit d'épargne collectif qui permet aux salariés d'une entreprise de se constituer un portefeuille de valeurs mobilières (actions, obligations, titres de créances négociables, ...). 

Le PEE se décline sous plusieurs noms selon son périmètre d'application :

- PEE (Plan d'épargne entreprise) : une entreprise
- PEG (Plan d'épargne groupe) : un groupe d'entreprises
- PEI (Plan d'épargne interentreprises) : plusieurs entreprises n'appartenant pas au même groupe

### Avantages

- Les sommes investies sont exonérées d'impôt sur le revenu lors d'un débloquage anticipé ou à la fin du plan (elles restent soumises aux prélèvements sociaux pour la part correspondant aux revenus générés par le plan). 
- Peut être alimenté par des abondements (versements de l'entreprise qui viennent compléter les versements des salariés).

### Inconvénients

- Les sommes investies dans le PEE sont bloquées pendant au moins 5 ans (sauf déblocage anticipé, cas : mariage, pacs, naissance, divorce, acquisition ou construction de la résidence princiaple, invalidité, décès, surendettement, ...).
- L'entreprise définit les actifs du plan.
- Frais du plan souvent élevés.

> Source [service-public.fr - F2142](https://www.service-public.fr/particuliers/vosdroits/F2142).

## Plan d'épargne pour la retraite collectif (PERCO)

Le plan d'épargne pour la retraite collectif (Perco) est un produit d'épargne d'entreprise. Si votre entreprise propose un Perco, il est ouvert à tous les salariés. Toutefois, une condition d'ancienneté peut être exigée (3 mois maximum).

### Avantages

- L'abondement de l'entreprise est exonéré d'impôt sur le revenu dans la limite de 7 039 €
- Les versements volontaires du salarié issus de l'intéressement et de la participation sont exonérés d'impôt sur le revenu dans la limite de 32 994 €.
- S'ils sont réinvestis dans le plan, les revenus des titres détenus dans le plan sont exonérés d'impôt sur le revenu.

### Inconvénients

- Les sommes versées sont bloquées jusqu'au départ à la retraite (sauf déblocage anticipé, cas : décès, invalidité, surendettement, acquisition ou remise en état de la résidence principale, expiration des droits chômage).
- L'entreprise définit les actifs du plan.
- Frais du plan souvent élevés.

> Source [service-public.fr - F10260](https://www.service-public.fr/particuliers/vosdroits/F10260)
