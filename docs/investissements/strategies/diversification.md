---
title: La diversification 
description: Qu'est-ce que la diversification ? Quels sont les avantages et inconvénients ?
---

# Diversification des actifs

## Définition

La diversification a pour objectif principal de réduire le risque et la volatilité de son portefeuille. Il est très recommandé de diversifier son portefeuille, quitte à baisser légèrement les performances à court terme.

Attention, la diversification ne supprime pas totalement les risques.

## Type de diversification

- Secteur d'activité (technologie, finance, santé, ...)
- Devise ($, €, ...)
- Actifs (ETF, action, obligation, matière première, ...)
- Zone géographique (US, Europe, Asie, ...)

## Cycle économique

| Valeurs cycliques | Valeurs défensives |
| ----------------- | ------------------ |
| BTP               | Agro alimentaire   |
| Banques           | Santé              |
| Automobile        | Chimie             |
| Médias            | Assurances         |
| Loisirs           | Télécomunications  |
| ...               | ...                |

## Aller plus loin

- [Zonebourse - Warren Buffett: "la diversification c'est pour les nuls"](https://www.youtube.com/watch?v=dmHq61o81HU)
- [Zonebourse - Débutants: ne vous faites plus surprendre !](https://www.youtube.com/watch?v=-c_cHe_yMuU)
