---
title: Allocation des actifs
description: Présentation de l'allocation des actifs avec des exemples.
---

# Allocation des actifs

## Définition

L'allocation d’actifs désigne la répartition du portefeuille de l'investisseur entre les différentes familles de placements.

Elle dépend notamment de votre niveau de risque. Une personne plus jeune aura souvent un niveau de risque plus élevé (vise la performance) qu'une personne âgée (préserver son capital).

## Exemple

### Morgan Stanley - Modèle 5

Morgan Stanley est une banque américaine qui a sorti 5 modèles d'allocations des actifs par ordre croissant du risque estimé.

Voici le modèle 5, autrement dit, pour un profil qui recherche la performance.

**Actions (~70%) :**

- US (29%)
- International (24%)
- Emerging market (8%)

**Alternatives (~20%) :**

- Alternative privée "Private Alts" (15%)
- Actifs réels "Real assets" (7%)

**Obligations (~10%) :**

- Obligation "Investment grade" (4%)
- Obligation à taux d'intérêt élevé (3%)


### Stratégie "All Weather"

La stratégie "All Weater" a été inventée par [Ray Dalio](https://en.wikipedia.org/wiki/Ray_Dalio).

Le but de ce portefeuille était de pouvoir résister aux différents régimes économiques en diversifiant son contenu en 4 parties (4 saisons).

Cette diversification est réalisée entre 4 classes d'actifs dîtes "décoréllés. C'est-à-dire, qu'elles ne sont pas liées entre elles et n'évoluent pas souvent dans le même sens.

Voici les 4 régimes économiques (4 saisons) :

1. Augmentation de l'inflation
2. Baisse de l'inflation
3. Augmentation de la croissance
4. Baisse de la croissance

![Inflation and Growth](https://ml4i65jzgymf.i.optimole.com/6NYgPcU-zpZNcPqn/w:auto/h:auto/q:eco/https://i0.wp.com/filippoangeloni.com/wp-content/uploads/2018/09/structural-diversification-heatmap.png?w=1200&ssl=1)

Voici l'allocation des actifs du portefeuille :

![Réparitition](https://2.bp.blogspot.com/-eVC4V6jw3bg/WVRj_PlvbKI/AAAAAAAAAEk/gSgTuGU3KDgjiMy1Uf5fgRHiGsWMLhKJQCLcBGAs/s1600/Picture5.png)

**Avantages :**

- Portefeuille diversifié.
- Fort sur le très long terme et assez résistant aux crises économiques.

**Inconvénients :**

- Performance moyenne. 

## Aller plus loin

- [Zonebourse - Allouer ses actifs comme un pro](https://www.youtube.com/watch?v=8_mpk8FzBp8)
- [Zonebourse - Préparer son portefeuille pour 2021](https://www.youtube.com/watch?v=xfzoYat1yoc)
- [Zonebourse - Le portefeuille "All Weather" de Ray Dalio](https://www.youtube.com/watch?v=8KDo8nNfewk)
- [PDF - Morgan Stanley, Global Investment Committee Asset Allocation Models](https://advisor.morganstanley.com/scott.altemose/documents/field/s/sc/scott-a--altemose/GIC%20Models%202_JUL%202021.pdf)

