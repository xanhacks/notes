---
title: Stratégies d'investissement
description: Définitions et exemples de différentes stratégies d'investissements.
---

# Stratégies d'investissements

## Règles d'or

1. Le futur est imprévisible, surtout à court terme. Les prévisions n'ont aucun intérêt.
2. La marché peut rester irrationnel plus longtemps que vous ne pouvez rester solvable.
3. N'essayer pas de "timer" (trouver le meilleur moment) le marché.
4. Plus d'argent est perdu à anticiper les krachs que pendant les krachs eux-mêmes.
5. Acheter en pleine crise est plus facile à dire qu'à faire. Même si vous pensez être prêt pour votre premier marché baissier ... vous ne l'êtes pas.
6. Les bulles paraissent toujours évidentes après coup, beaucoup moins au moment où elles ont lieu.
7. La chance et le talent sont souvent confondus.
8. La diversification offre la meilleure protection contre l'impossibilité de prévoir le futur.
9. L'allocation des actifs a beaucoup plus d'importance que la sélection d'actif dans la performance de votre portefeuille.
10. L'investisseur passif moyen bat l'investisseur actif moyen à long terme.
11. Plus vous investissez à long terme, plus vos chances de succès sont importantes.
12. Les qualités les plus utiles pour un investisseur sont la discipline, la patiente et l'humilité.
13. Il n'y a pas de rendement sans risque. Si les rendements parasissent trop beaux pour être vrais, c'est probablement une arnaque.
14. Evitez l'effet de levier.
15. La meilleure stratégie est celle que vous êtes capable de tenir à long terme.
16. Les intérêts composés sont l'une des forces les plus puissantes du marché.
17. Epargner est une plus grosse priorité qu'investir.
18. L'étude de la psychologie et de l'histoire boursière est plus utile pour les investisseurs que celle de l'économie. Il y a très peu de corrélation à court terme entre l'économie et les marchés boursiers.

## Trading vs Long terme

**Trading :** Achat et revente dans un intervalle de temps réduit. Méthode risquée et déconseillée.

**Long terme :** L'investissement à long terme est beaucoup moins risqué et permet de gagner beaucoup d'argent grâce aux intérêts composés (effet boule de neige). Cependant, cette méthode doit être maintenue pendant plusieurs années, voire des décénnies afin de pouvoir en profiter au maximum.

## DCA - Dollar Cost Averaging

### Définition

Investir régulièrement (ex: tous les mois) la même somme d'argent.

Le Dollar Cost Averaging fonctionne parce qu'à long terme, les prix des actifs ont tendance à croître. Nombreux sont ceux qui ont tenté d’anticiper le marché et d’acheter des actifs lorsque leurs prix semblent bas. Cela semble assez facile, en théorie. En pratique, il est presque impossible, même pour les professionnels, de déterminer comment le marché va évoluer à court terme.

### Avantages

- Simple.
- Se séparer des émotions.

### Inconvénients

- Fractionnement d'actifs parfois obligatoire en cas d'actif onéreux.

## Aller plus loin 

- [Zonebourse - 18 règles d'or en bourse](https://www.youtube.com/watch?v=1yUpiPEG60E)

## Momentum

### Définition

La stratégie de momentum en investissement financier consiste à acheter des actifs qui ont récemment eu des performances élevées et à vendre ou à éviter les actifs qui ont récemment eu des performances faibles. En résumé, cette stratégie consiste à suivre les tendances du moment.

### Avantages

- Possibilité de surperformer le marché si les tendances de momentum se poursuivent
- Réduction des risques si les investisseurs vendent des actifs qui ont commencé à mal performer

### Inconvénients

- Peut entraîner des achats dans des actifs surcotés

