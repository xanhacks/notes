---
title: Types d'ordres
description: Présentation des différents d'ordres bancaire. 
---

# Types d'ordres

## Au marché

L'ordre au marché revient à dire : « J'achète / vends sans me positionner sur un prix, mais selon le prix fixé par les contreparties présentes ».

Cet ordre est prioritaire sur tous les autres et sera exécuté au prix fixé par le marché. Cependant, l'ordre au marché n'offre aucune maîtrise sur le prix auquel sera effectuée la transaction. Plus la liquidité d'un actif est faible, plus cet ordre peut s'avérer risqué.

## Cours limité

L'ordre à cours limité consiste à passer un ordre d'achat (ou de vente) avec un prix d'achat maximum (ou un prix de vente minimum). Cet ordre présente l'inconvénient de ne pas être exécuté si le cours limite n'est pas atteint (ou bien de n’être exécuté que partiellement).

## Seuil de déclenchement

L’ordre à seuil de déclenchement (ex: ordres STOP) est exécuté également à partir d’un cours déterminé, pour limiter les pertes en cas de vente ou réaliser un achat en cas de hausse.

## Plage de déclenchement

L’ordre à plage de déclenchement est exécuté dans une fenêtre de prix déterminée à l’avance. On détermine ainsi un cours minium et un cours maximum de déclenchement.

## Suiveur

L'ordre suiveur fonctionne comme un ordre à seuil de déclenchement sauf que l’on peut ici saisir un pourcentage de variation et non un seuil de prix.
