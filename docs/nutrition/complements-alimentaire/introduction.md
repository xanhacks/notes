---
title: Compléments alimentaires
description: Explication des compléments alimentaires avec les avantages, risques, doses, ...
---

# Compléments alimentaires

Les compléments alimentaires sont des produits préparés, qui contiennent une ou plusieurs substances, destiné à être pris en complément de l’alimentation.

En France, la composition et les doses maximales des compléments alimentaires sont encadrées par le [décret n°2006-352](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000638341/).

> Les compléments alimentaires ne sont pas des substituts de repas ou même, ne peuvent remplacer une alimentation équilibrée.

## Contenances

Les compléments alimentaires sont souvent séparés en 3 catégories :

- Vitamines et minéraux
- Acides aminés et protéines
- Autres (extraits de plantes, ...)

## Sécurité

Il existe des interactions possibles entre des substances contenues dans certains compléments alimentaires et les médicaments (mélatonine, oméga 3, etc.)

L’Agence nationale de sécurité sanitaire de l’alimentation, de l’environnement et du travail (ANSES) souligne que pour consommer des compléments alimentaires, il est important :

- d’éviter des prises prolongées, répétées ou multiples au cours de l’année sans demander conseil à un professionnel de santé ;
- de respecter scrupuleusement les conditions d’emploi fixées par le fabricant, responsable de la sécurité des produits qu’il commercialise ;
- de signaler à un professionnel de santé tout effet indésirable survenant suite à la consommation d’un complément alimentaire ;
- de privilégier les circuits d’approvisionnement contrôlés par les pouvoirs publics.


## Bibliographie

- https://www.ameli.fr/assure/sante/medicaments/effets-secondaires-et-interactions-lies-aux-medicaments/complements-alimentaires
- https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000638341/