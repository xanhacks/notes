---
title: Protéines en poudre
description: Informations liées aux protéines en poudre, objectifs, doses, avantages, risques, ...
---

# Protéines en poudre

## Objectif

Ce type de complément alimentaire est souvent utilisé par les sportifs de haut niveau pour développer plus rapidement leurs masses musculaires.

## Doses

Selon les études la dose recommandée pour les sportifs varie légèrement, elle est d'environ **1.5g de protéines par kilo de poids de corps par jour**.

L'étude [Dietary protein intake and human health (DOI 10.1039/C5FO01530H)](https://pubs.rsc.org/en/content/articlehtml/2016/fo/c5fo01530h) recommande une dose en gramme de protéines par kilo de poids de corps par jour de :

- 1.0g/kg BW/day : Individu avec une activité physique légère.
- 1.3g/kg BW/day : Individu avec une activité physique modérée.
- 1.6g/kg BW/day : Individu avec une activité physique intense.

L'étude rajoute : Pour des adultes en bonne santé, une consommation de protéines sur le long terme inférieur à 2.0g/kg BW/day n'est pas dangereuse.

> Attention, une surconsommation peut engendrer des anomalies digestives, rénales et vasculaires.

## Types de protéines en poudre

- **Protéine de lactosérum (Whey)** : La protéine de lactosérum vient tout d’abord du lait, mais plus précisément du petit-lait, qui est le résidu liquide de la fabrication du fromage et d’autres produits laitiers. 
- **Isolat de lactosérum (Whey Isolate)** : La protéine appelée Isolate est une protéine qui a été filtrée et traitée à partir de la protéine de Whey afin de supprimer le gras, les glucides ainsi que le lactose.
- **Protéine végétale** : Provient principalement du riz, des pois et du chanvre.

Source [shopsante.ca](https://shopsante.ca/blogs/news/les-differents-types-de-proteines-en-poudre).

## Bibliographie

> Athletes whose protein offer of 2.5g/kg weight/day did not bring additional benefit to 1.5g/kg/day to increase the protein flow and synthesis. [2012 - Protein consumption by bodybuilding practitioners aiming muscle hypertrophy (DOI 10.1590/S1517-86922012000100001)](https://www.scielo.br/j/rbme/a/sFDmRDBJMYVngCCcJtGgNsk/?lang=en)

> While protein restriction may be appropriate for treatment of existing kidney disease, we find no significant evidence for a detrimental effect of high protein intakes on kidney function in healthy persons after centuries of a high protein Western diet. [2005 - Dietary protein intake and renal function (DOI 10.1186/1743-7075-2-25)](https://link.springer.com/article/10.1186/1743-7075-2-25)

> To meet the functional needs such as promoting skeletal-muscle protein accretion and physical strength, dietary intake of 1.0, 1.3, and 1.6 g protein per kg BW per day is recommended for individuals with minimal, moderate, and intense physical activity, respectively. Long-term consumption of protein at 2 g per kg BW per day is safe for healthy adults, and the tolerable upper limit is 3.5 g per kg BW per day for well-adapted subjects. Chronic high protein intake (>2 g per kg BW per day for adults) may result in digestive, renal, and vascular abnormalities and should be avoided. The quantity and quality of protein are the determinants of its nutritional values. Therefore, adequate consumption of high-quality proteins from animal products (e.g., lean meat and milk) is essential for optimal growth, development, and health of humans. [2016 - Dietary protein intake and human health (DOI 10.1039/C5FO01530H)](https://pubs.rsc.org/en/content/articlehtml/2016/fo/c5fo01530h)