---
title: Créatine
description: Informations liées aux protéines en poudre, objectifs, doses, avantages, risques, ...
---

# Créatine

## Objectif

La créatine monohydrate est un complément alimentaire qui augmente la performance musculaire lors d'exercices de résistance de courte durée et de haute intensité. Cependant, ni la performance des exercices d'endurance ni l'absorption maximale d'oxygène ne semblent être améliorées.

## Doses

Il est possible de commencer par une phase de charge (non obligatoire), puis de passer à la phase de lissage.

1. Phase de charge (5 à 7 jours) : 0.3 g per BW kilo per day
2. Phase de lissage : 0.03 g per BW kilo per day

Pour une personne de 80 kg, la phase de charge est à 24g et la phase de lissage à 2.4g de créatine monohydrate par jour.

## Bibliographie

> The effective dosing for creatine supplementation includes loading with 0.3 g·kg−1·d−1 for 5 to 7 days, followed by maintenance dosing at 0.03 g·kg−1·d−1 most commonly for 4 to 6 wk. [...] The most common adverse effect is transient water retention in the early stages of supplementation. When combined with other supplements or taken at higher than recommended doses for several months, there have been cases of liver and renal complications with creatine. [2013 - Creatine Supplementation (DOI 10.1249/JSR.0b013e31829cdff2)](https://journals.lww.com/acsm-csmr/fulltext/2013/07000/creatine_supplementation.10.aspx)

> No adverse effects have been identified with short term creatine feeding. [1994 - Creatine in Humans with Special Reference to Creatine Supplementation (DOI 10.2165/00007256-199418040-00005)](https://link.springer.com/article/10.2165/00007256-199418040-00005)

> The present review is not intended to reach conclusions on the effect of creatine supplementation on sport performance, but we believe that there is no evidence for deleterious effects in healthy individuals. Nevertheless, idiosyncratic effects may occur when large amounts of an exogenous substance containing an amino group are consumed, with the consequent increased load on the liver and kidneys. Regular monitoring is compulsory to avoid any abnormal reactions during oral creatine supplementation. [2000 - Adverse Effects of Creatine Supplementation (DOI 10.2165/00007256-200030030-00002)](https://link.springer.com/article/10.2165/00007256-200030030-00002)

> Short-term use of creatine is considered safe and without significant adverse effects, although caution should be advised as the number of long-term studies is limited. Suggested dosing is variable, with many different regimens showing benefits. The safety of creatine supplementation has not been studied in children and adolescents. [2017 - Creatine Use in Sports (DOI 10.1177/1941738117737248)](https://journals.sagepub.com/doi/abs/10.1177/1941738117737248)