---
title: Collagen
description: Informations liées au collagen
---

# Collagen

## Introduction

Le collagène est une protéine présente dans le corps de tous les animaux, y compris les humains. Il constitue le tissu conjonctif, comme la peau, les tendons, le cartilage, les organes et les os.

## Objectifs

La supplémentation en collagène est utilisée pour :

- préservation de la structure de la peau
- cicatrisation des plaies
- anti-vieillissement
- l'ostéoarthrite (douleurs articulaires)s
- prévention de la perte osseuse
- amélioration de la masse musculaire
- santé des cheveux et des ongles

## Doses

La dose recommandée de collagen hydrolysé est comprise entre 2.5 à 15 g par jour.

## Bibliographie

> The aim of this study was to conduct a double-blind, randomized, placebo-controlled trial to clinically evaluate the effect on human skin hydration, wrinkling, and elasticity of Low-molecular-weight Collagen peptide (LMWCP) with a tripetide (Gly-X-Y) content >15% including 3% Gly-Pro-Hyp. Individuals (n = 64) were randomly assigned to receive either placebo or 1000 mg of LMWCP once daily for 12 weeks. [...] These results suggest that LMWCP can be used as a health functional food ingredient to improve human skin hydration, elasticity, and wrinkling. [Oral Intake of Low-Molecular-Weight Collagen Peptide Improves Hydration, Elasticity, and Wrinkling in Human Skin: A Randomized, Double-Blind, Placebo-Controlled Study (DOI 10.3390/nu10070826)](https://www.mdpi.com/2072-6643/10/7/826/htm)

> The purpose of this study was to investigate the effects of supplementation of whey protein (WP) versus leucine-matched collagen peptides (CP) on muscle thickness (MT) and performance after a resistance training (RT) program in young adults. [...] Supplementation with WP was superior to leucine content-matched CP supplementation in increasing muscle size, but not strength and power, after a 10-week RT program in young adults. [Whey Protein Supplementation Is Superior to Leucine-Matched Collagen Peptides to Increase Muscle Thickness During a 10-Week Resistance Training Program in Untrained Young Adults (DOI 10.1123/ijsnem.2021-0265)](https://journals.humankinetics.com/view/journals/ijsnem/32/3/article-p133.xml)

> The results of twenty years of research indicate that the inclusion of collagen peptides in the diet can lead to various improvements in health. [...] This study suggests that the effective amounts of functional collagen peptides (2.5 to 15 g per day) observed in the literature are below the maximum level of collagen that may be incorporated in the standard American diet. [Significant Amounts of Functional Collagen Peptides Can Be Incorporated in the Diet While Maintaining Indispensable Amino Acid Balance (DOI 10.3390/nu11051079)](https://pubmed.ncbi.nlm.nih.gov/31096622/)

> Healthline : [Is Hydrolyzed Collagen a Miracle Cure?](https://www.healthline.com/health/food-nutrition/is-hydrolyzed-collagen-a-miracle-cure) & [How Much Collagen Should You Take per Day?](https://www.healthline.com/nutrition/how-much-collagen-per-day)