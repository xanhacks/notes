---
title: Caféine
description: Informations liées à la caféine
---

# Caféine

## Introduction

La caféine (1,3,7-triméthylxanthine) est la substance pharmacologique et psychoactive la plus consommée au monde. On la trouve naturellement dans les grains de café, les feuilles de thé, le chocolat, les fèves de cacao et les noix de cola.

Quantité de cafiné par tasse de :

- Café : 60 à 150 mg
- Thé : 40 à 60 mg
- Cola (boissons gazeuses) : 40 à 50 mg

> Source [Caffeine Use in Sports: Considerations for the Athlete (DOI 10.1519/JSC.0b013e3181660cec)](https://journals.lww.com/nsca-jscr/Fulltext/2008/05000/Caffeine_Use_in_Sports__Considerations_for_the.47.aspx)

## Objectif

La caféine, bien qu'elle n'ait aucune valeur nutritionnelle, a attiré l'attention de nombreux athlètes de compétition et non compétitifs en tant qu'aide ergogénique (substances qui améliorent la performance) légale.

## Doses

La dose "recommandée" est de 3 mg de caféine par kilo de poids du corps.

Pour un invidu de 80 kg, nous obtenons 240 mg de caféine par jour, soit environ 2-3 tasses de café.

## Bibliographie

> The available literature that follows such guidelines suggests that performance benefits can be seen with moderate amounts (~3 mg·kg–1 body mass) of caffeine. Furthermore, these benefits are likely to occur across a range of sports, including endurance events, stop-and-go events (e.g., team and racquet sports), and sports involving sustained high-intensity activity lasting from 1–60 min (e.g., swimming, rowing, and middle and distance running races). The direct effects on single events involving strength and power, such as lifts, throws, and sprints, are unclear. [Caffeine and sports performance (DOI 10.1139/H08-130)](https://cdnsciencepub.com/doi/abs/10.1139/H08-130?journalCode=apnm)

> The pleasant stimulant feeling which often occurs at low doses may be replaced by psychological symptoms which resemble anxiety and depressive neuroses at high doses. [Caffeine Psychological
Effects, Use and Abuse](https://isom.ca/wp-content/uploads/2020/01/JOM_1981_10_3_09_Caffeine_Psychological_Effects_Use_and_Abuse.pdf)

> We conclude that regular consumption of 12 mg · kg−1 of caffeine per day (equivalent to approximately 6 to 11 cups of coffee per day) may produce pharmacodynamic effects not completely compensated for by the development of tolerance. [Effects of caffeine with repeated dosing (DOI 10.1007/BF00315208)](https://link.springer.com/article/10.1007/BF00315208)