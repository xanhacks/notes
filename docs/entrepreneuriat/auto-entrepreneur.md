---
title: Auto-entrepreneur
description: Comment créer et gérer une auto-entreprise.
---

# Auto-entrepreneur

!!! warning

	Les informations présentes sur cette page peuvent être inexactes ou obsolètes. 

## Introduction

Le micro-entrepreneur, ou auto-entrepreneur, bénéficie d'un régime unique et simplifié, destiné à faciliter un début d'activité.

Pour bénéficier du régime de la micro-entreprise, votre chiffre d'affaires ou vos recettes doivent être en dessous des seuils suivants :

- 188 700 € pour les activités de commerce et de fourniture de logement. 
- 77 700 € pour les activités de prestations de services ou les activités libérales. 

> [Micro-entrepreneur- Service public](https://entreprendre.service-public.fr/vosdroits/F23961)

## Obligations légales

1. **Déclarer votre activité et votre chiffre d'affaires :** vous devez remplir et envoyer chaque mois ou chaque trimestre une déclaration de chiffre d'affaires sur le site [www.autoentrepreneur.urssaf.fr](https://www.autoentrepreneur.urssaf.fr).
2. **Payer vos cotisations sociales et vos impôts :** vous devez payer des cotisations sociales (maladie, retraite, chômage) et des impôts sur le revenu (si vous dépassez un certain seuil de chiffre d'affaires). Le montant de ces cotisations et impôts dépend de votre activité et de votre chiffre d'affaires. Vous pouvez bénéficier d'un abaissement des cotisations si vous possédez l'ACRE.
3. **Tenir une comptabilité simplifiée :** vous devez enregistrer toutes les entrées et sorties d'argent de votre entreprise. Vous devez également conserver tous les justificatifs de vos dépenses et de vos recettes (factures, tickets de caisse, etc.) pendant 6 ans.
4. Si, au cours de votre activité, votre chiffre d'affaires annuel dépasse 10 000 € durant 2 années consécutives, vous aurez l'obligation de créer un [compte dédié](https://entreprendre.service-public.fr/vosdroits/F35991) à votre activité professionnelle.

> [Obligations comptables du micro-entrepreneur - Service public](https://entreprendre.service-public.fr/vosdroits/F23266)

## ACRE (Aide à la création ou à la reprise d'une entreprise)

L'aide à la création ou à la reprise d'une entreprise (Acre) consiste en une exonération partielle de charges sociales. L'exonération intervient uniquement la première année d'activité.

Le taux des cotisations à payer pendant la période de l'ACRE dépend de l'activité de l'entreprise :

- Vente de marchandises (BIC) : 6,4 % (au lieu de 12,8 %)
- Prestations de services artisanales ou commerciales : 11 % (au lieu de 22 %)
- Activité libérale : 11 % (au lieu de 22 %)

> [ACRE - Service public](https://www.service-public.fr/particuliers/vosdroits/F11677)

## Franchise de TVA

La franchise en base de TVA exonère les entreprises de la déclaration et du paiement de la TVA sur les prestations ou ventes qu'elles réalisent. Pour bénéficier du régime de la franchise en base de TVA votre chiffre d'affaires ne doit pas dépasser certains seuils. Ces seuils sont différents selon l'activité que vous exercez.

Pour bénéficier de la franchise en base de TVA pour une activité libérale (sauf avocat), vous devez être être dans l'une des situations suivantes :

- Votre chiffre d'affaires de l'année civile précédente ne doit pas dépasser 36 800 €
- Votre chiffre d'affaires de l'avant-dernière année civile ne doit pas dépasser 36 800 € et celui de l'année civile précédente ne doit pas dépasser 39 100 €
- Votre chiffre d'affaires de l'année civile en cours ne doit pas dépasser 39 100 €. Si vous dépassez ce seuil, vous devrez payer la TVA le 1er jour du mois de dépassement

> [Franchise en base de TVA - Service public](https://entreprendre.service-public.fr/vosdroits/F21746)

## Impôts

Sur le plan fiscal, vous avez le choix entre :

- le régime classique (régime micro-fiscal) - (choix par défaut)
- le régime optionnel de versement libératoire

### Imposition sur le revenu

Vous n'avez pas à fournir une déclaration professionnelle de bénéfices pour vos BNC ou BIC. Il faut simplement que vous ajoutiez vos bénéfices dans votre déclaration complémentaire de revenu ([n°2042-C Pro](https://entreprendre.service-public.fr/vosdroits/R36751)) :

- Le montant annuel de votre chiffre d'affaires brut (BIC) devra être indiqué dans la partie « Revenus industriels et commerciaux professionnels »
- Le montant de vos recettes (BNC) devra être indiqué dans la partie « Revenus non commerciaux »

Pour calculer le bénéfice imposable, un abattement forfaitaire est appliqué à votre chiffre d'affaires. Les taux de cet abattement diffèrent en fonction de l'activité exercée par l'entreprise  :

- 71 % du CA : Activités relevant des BIC (achat-revente ou de fourniture de logement)
- 50 % du CA : Activités relevant des BIC (prestations de service)
- 34 % du CA : Activités relevant des BNC (revenus non commerciaux)

A savoir :

- L'abattement ne peut pas être inférieur à 305 €.
- En cas d'activités mixtes, les abattements sont calculés séparément pour chaque fraction du chiffre d'affaires qui correspond aux activités exercées. Dans ce cas, la déduction minimale est de 610 €.
- Les impôts dans le régime de l'autoentreprise ne sont pas déductibles de vos dépenses professionnelles. Cela signifie que vous ne pouvez pas déduire vos frais professionnels (achat de matériel, frais de déplacement, etc.) de votre chiffre d'affaires avant de calculer l'impôt que vous devez payer.

Une fois calculé, le bénéfice imposable est soumis à [l'impôt sur le revenu](https://www.service-public.fr/particuliers/vosdroits/F34328) avec les autres revenus du foyer fiscal.

> [Imposition du micro-entrepreneur - Serivce public](https://entreprendre.service-public.fr/vosdroits/F23267)<br>
> [Quels impôts pour un auto-entrepreneur ? - Legalplace](https://www.legalplace.fr/guides/auto-entrepreneur-impots/)

## CFE (Cotisation foncière des entreprises)

Un micro-entrepreneur, qu'il possède un local ou non, doit payer la cotisation foncière des entreprises (CFE) dans les mêmes conditions que tout créateur d'entreprise.

- Une entreprise est exonérée de CFE l'année de sa création (uniquement jusqu'au 31 décembre de l'année en cours). Ensuite, sa base d'imposition est réduite de moitié l'année suivante.
- L'entreprise est exonérée de cotisation minimum si son chiffre d'affaires annuel ne dépasse pas 5 000 €.

Cotisation minimum due en 2022 en fonction du chiffre d'affaires de l'année N-2 :

| Chiffre d'affaires réalisé en N-2 |CFE minimum due en 2022 (selon la commune) |
| --------------------------------- | ----------------------------------------- |
| Entre 5 001 € et 10 000 €         | Entre 227 € et 542 €                      |
| Entre 10 001 € et 32 600 €        | Entre 227 € et 1 083 €                    |
| Entre 32 601 € et 100 000 €       | Entre 227 € et 2 276 €                    |
| Entre 100 001 € et 250 000 €      | Entre 227 € et 3 794 €                    |
| Entre 250 001 € et 500 000 €      | Entre 227 € et 5 419 €                    |
| À partir de 500 001 €             | Entre 227 € et 7 046 €                    |

> [Cotisation foncière des entreprises (CFE) - Service public](https://entreprendre.service-public.fr/vosdroits/F23547)
