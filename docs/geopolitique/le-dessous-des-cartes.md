# Le dessous des cartes

> Cette page contient une prise de note sur le livre [Le Dessous des Cartes - Le retour de la guerre](https://boutique.arte.tv/detail/le-dessous-des-cartes-le-retour-de-la-guerre).

## I. EUROPE - Le temps des crises

### Russie-Ukraine : la dernière guerre de Vladimir Poutine ?

Vladimir Poutine justifie l'invasion de l'Ukraine par son projet de faire reculer l'OTAN qui se serait rapprocher des frontières de la Russie et menacerait leur sécurité. Poutine a déja utilisé cette justification en 2008 lors d'une intervention en Géorgie et l'annexion de la Crimée en 2014.

**La Russie**

- Un siège permanant au Conseil de sécurité de l'[ONU](https://fr.wikipedia.org/wiki/Organisation_des_Nations_unies)
- Puissance politique étrangère offensive
- Absence de pluralisme politique
- Précarité économique croissante de la population (11ème PIB mondial)
- Frontalière avec une quinzaine d'Etats (dont le les Etats-Unis avec le détroit de Béring, l'océan Glacial arctique, la Chine et l'UE)
- [Site spacial au Kazakhstan](https://fr.wikipedia.org/wiki/Cosmodrome_de_Ba%C3%AFkonour)

**Kremlin**

Ensemble architectural complexe situé au sein d’une forteresse du centre de Moscou, il contient Le Grand Palais et le palais du Sénat où siège l'administration présidentielle Russe. Par métonymie, l'expression « le Kremlin » désigne souvent, dans la presse et les médias, le pouvoir russe ou, précédemment, le pouvoir soviétique. 

**Révolution orange (2004)**

- Série de manifestations politiques en Ukraine dû à l'élection du président pro-russe, car perçu comme truqué par les Ukrainiens.
- Moscou réplique en 2006 en interrompant l'approvisionnement de Gaz à l'Ukraine (arme énergétique via l'entreprise Gazprom).

**Moyen-Orient**

- La Russie soutient Bachar-el-Assad en 2015.

**Arctique**

- La Russie possède la plus longue facade maritime sur l'Arctique.
- Plusieurs bases militaires russes sont installées.
- Route maritime de grande importance.
- Contient de forte réserves pétrolières et de gaz.
- Conformément au droit de la mer, l'arctique est partagé avec le Canada, le Groenland (Danemark) et les Etats-Unis.

**Guerre numérique**

- Cyberpropagande lors des élections américaines de 2016 et française en 2017.
- Financement dans les médias (chaîne de TV "Russia Today" et agence de presse "Sputnik").

**Opposition politiques**

- Assassinat de [Boris Nemtsov](https://fr.wikipedia.org/wiki/Boris_Nemtsov) en 2015 (opposant politique).
- Tentative d'empoisonnement de [Sergueï Skripal](https://fr.wikipedia.org/wiki/Sergue%C3%AF_Skripal) (agent double Anglais et Russe) en 2018.
- Empoisonnement et arrestation d'[Alexeï Navalny](https://fr.wikipedia.org/wiki/Alexe%C3%AF_Navalny) (avocat, opposant de Poutine) en 2020-2021.

**Guerre en Ukraine**

- Pays composé de russophone/russophile et l'« autre Ukraine » tournée vers l'UE, la démocratie et la liberté. 
- Offensive russe en Ukraine le 24/02/2022.
- Les Ukrainiens ripostent avec l'aide des US et l'OTAN.
- Important déplacement de populations vers les pays voisins (Pologne, Russie, Roumanie, ...).
- Conflit polymorphe (militaire, idéologique, civilisationnel)
