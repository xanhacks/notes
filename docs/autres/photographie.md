# Photographie

## Transition

- Tapez dans la main de quelqu'un
- Mettre/enlever sa main sur la caméra rapidement

## Scène

- Hyperaccéléré en avant/en arrière
- Dome Telesin (passage de contenu terrestre à sous-marin)
- Timelapse de paysage ou de nuit
- Ralenti (slow motion)
- Filmer avec un trépied
- Suivre un personnage en mouvement
- Suivre le sol avec la caméra
- Point de vue (POV)
- Match cut, faire demi-tour avec une perspective selfie

## Conseils

- Choisir une musique qui met de l'ambiance