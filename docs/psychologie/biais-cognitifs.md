# Biais cognitifs

## Définition
Un **biais cognitif** est une déviation dans le traitement cognitif d'une information. Le terme _biais_ fait référence à une déviation systématique de la pensée logique et rationnelle par rapport à la réalité.

![Liste des biais cognitifs](https://upload.wikimedia.org/wikipedia/commons/1/16/The_Cognitive_Bias_Codex_%28French%29_-_John_Manoogian_III_%28jm3%29.svg?uselang=fr)

## Exemples

Le [**phénomène Baader-Meinhof**](https://fr.wikipedia.org/wiki/Illusion_de_fr%C3%A9quence) (ou **illusion de fréquence**) survient lorsqu'une personne, après avoir fait connaissance pour la première fois d'un fait, d'un mot, d'un phénomène ou tout autre chose, rencontre à nouveau, parfois à plusieurs reprises, cette même chose peu de temps après sa découverte. Ce phénomène peut se rapprocher de la paramnésie ou phénomène de "déjà-vu".

L'[**oubli de la fréquence de base**](https://fr.wikipedia.org/wiki/Oubli_de_la_fr%C3%A9quence_de_base) est un biais cognitif lié aux lois statistiques, qui se manifeste par le fait que les gens oublient souvent de considérer la fréquence de base de l'occurrence d'un événement lorsqu'ils cherchent à en évaluer une probabilité.

Le [**biais de confirmation**](https://fr.wikipedia.org/wiki/Biais_de_confirmation) est notre tendance à sélectionner uniquement les informations qui confirment des croyances ou des idées préexistantes. Il sera encore plus prononcé dans des contextes idéologiques, politiques ou les contextes sociaux chargés d’émotions.

Le concept de [**post-rationalisation**](https://www.definitions-marketing.com/definition/post-rationalisation/) fait référence au fait qu'un individu ou un client va considérer à posteriori qu'une décision / un achat a été effectué de manière rationnelle alors qu'en fait cette décision à plutôt été prise sous forme d'une émotion ou d'une impulsion.

Les [**preuves anecdotiques**](https://fr.wikipedia.org/wiki/Preuve_anecdotique) sont des preuves tirées d'anecdotes, c'est-à-dire des preuves recueillies de façon occasionnelle ou informelle, s'appuyant fortement ou entièrement sur des témoignages personnels, et pouvant nourrir un raisonnement fallacieux appelé **argumentation par l'exemple**.

L'effet [**pom-pom girl**](https://journals.sagepub.com/doi/abs/10.1177/0956797613497969), nous sommes tous plus attirants en groupe quel seul.

Le [**biais rétrospectif**](https://fr.wikipedia.org/wiki/Biais_r%C3%A9trospectif) désigne la tendance qu'ont les personnes à surestimer rétrospectivement le fait que les événements auraient pu être anticipés moyennant davantage de prévoyance ou de clairvoyance. C'est un mécanisme de déni du hasard dans lequel tout événement doit pouvoir se justifier afin d’être le plus prévisible possible (sentiment de contrôler l'incertitude).

L['effet Ikea](https://fr.wikipedia.org/wiki/Effet_Ikea), appelé aussi **effet de possession**, est le nom donné à un biais cognitif dans lequel les consommateurs accordent une valeur disproportionnée aux produits qu'ils ont partiellement créés. Le terme provient du nom du fabricant et vendeur de meubles suédois Ikea, qui vend de nombreux produits en kit nécessitant d'être assemblés.

Le **biais de croyance** consiste en la formation d’hypothèses et la prise de décisions en fonction de ce que l’on désire et que l’on se plaît à imaginer au lieu de prendre en compte l’évidence, la rationalité et la réalité.

L’[**aversion à la dépossession**](https://fr.wikipedia.org/wiki/Aversion_%C3%A0_la_d%C3%A9possession) ou l’**effet de dotation** est une hypothèse selon laquelle les gens donnent plus de valeur à un bien ou un service lorsque celui-ci est leur propriété. Autrement dit, plus de valeur est attribuée à une même chose lorsqu’elle nous appartient que lorsqu'elle ne nous appartient pas.