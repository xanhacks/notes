---
title: Bienvenue 
description: Description de l'objectif du site web.
---

# Des notes pour plus tard 

## Objectif

C'est ennuyeux d'oublier certaines choses que l'on a apprises il y a quelque temps. C'est pour cette raison que j'ai décidé de lancer ce site de documentation nommé "Des notes pour plus tard". Le but est de pouvoir revenir y jeter un coup d'oeil quand j'ai un trou de mémoire.

## Catégories

- **Bien-être :** Bonheur, santé mentale, connaissance de soi, ...
- **Investissements :** Les bases de l'investissement financier, les types d'actifs et de comptes, les stratégies, l'analyse financières, ...
- **Nutrition :** Macronutriments, micronutriments, vitamines, oligo-éléments, compléments alimentaires ...

## Nota bene

Je m'excuse par avance pour les fautes d'orthographe qui se glisseront dans mes phrases.

J'essaie de sourcer au maximum ce que j'écris pour éviter de dire des bêtises. Si vous voyez une incohérence ou une erreur n'hésitez pas à me le faire savoir.

Bonne lecture !
