# Éducation Nationale

En France, l'éducation nationale comprend l'enseignement primaire, secondaire et supérieur. Il est géré par le ministère de l'Education nationale et est gratuit pour tous les enfants et les jeunes jusqu'à un certain âge. L'objectif principal est de fournir une éducation de qualité à tous les citoyens, indépendamment de leur origine sociale ou économique.

En 2020, il y avait environ 1 100 000 personnes travaillant dans l'éducation nationale en France, dont environ 870 000 enseignants. Le budget de l'éducation nationale en France est évalué à environ 157 milliards d'euros (6.7% du PIB). 

L'école gratuite et obligatoire en France a été instaurée par Jules Ferry en 1881 pour tous les enfants âgés de 6 à 13 ans. Il était alors ministre de l'Instruction publique. Cette réforme a permis d'augmenter considérablement l'alphabétisation et l'accès à l'éducation pour les enfants des classes populaires.

Malgré de nombreuses réformes, les inégalités entre les élèves restent présentes. Les inégalités socio-économiques, culturelles et territoriales continuent de peser sur l'égalité des chances éducatives.

La baisse d'attractivité du métier de professeur est un autre défi pour l'éducation nationale. Ce phénomène est dû à un certain nombre de facteurs tels que :

- la perte de pouvoir d'achat des enseignants
- les difficultés lors des premières années d'exercice
- le sentiment d'être peu écouté par la hiérarchie

Il est important de souligner que l'éducation stricte n'est pas la solution pour améliorer l'éducation nationale. Cela peut avoir un impact négatif sur la santé mentale des élèves et augmenter les taux de suicide, comme c'est le cas en Corée du Sud et au Japon. En Chine, les enfants pauvres sont écartés du système scolaire. Pour améliorer l'éducation des élèves, il est important de mettre en place des méthodes pédagogiques telles que :

- des classes à petit effectif
- des rofesseurs experts en pédagogie
- du travail en groupe et la liberté éducative pour les professeurs

> [L'impossible transformation de l'Éducation Nationale](https://www.youtube.com/watch?v=EiKIwERRnc0)<br>
> [L'éducation nationale en chiffres 2020](https://www.education.gouv.fr/l-education-nationale-en-chiffres-2020-305457)
